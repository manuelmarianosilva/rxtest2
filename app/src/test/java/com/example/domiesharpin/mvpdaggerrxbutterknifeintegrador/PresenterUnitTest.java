package com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador;

import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.MainActivityElements.CustomViewModel;
import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.MainActivityElements.MainActivityPresenter;
import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.MainActivityElements.MovieMVP;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;


public class PresenterUnitTest {

    MainActivityPresenter mainActivityPresenter;
    Observable<CustomViewModel> viewModelObservable;
    CustomViewModel model;

    MovieMVP.MainActivityView mockedView;
    MovieMVP.MainActivityModel mockedModel;

    @Before
    public void initializeTest(){

        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) {
                return Schedulers.trampoline();
            }
        });

        mockedView = mock(MovieMVP.MainActivityView.class);
        mockedModel = mock(MovieMVP.MainActivityModel.class);

        model = new CustomViewModel("Cacho Lives", "Gonzalez Catán");

        viewModelObservable = Observable.just(model);

        mainActivityPresenter = new MainActivityPresenter(mockedModel);
        mainActivityPresenter.setView(mockedView);
    }

    @Test
    public void varifyCallIsMadeAndPassedToTheView(){
        when(mockedModel.result()).thenReturn(viewModelObservable);
        mainActivityPresenter.loadData();
        verify(mockedModel,times(1)).result();
        verify(mockedView,times(1)).updateData(model);
    }

    @Test
    public void verifyNothingShowsOnViewIfCallToNetworkIsEmpty(){
        when(mockedModel.result()).thenReturn(Observable.<CustomViewModel>empty());
        mainActivityPresenter.loadData();
        verify(mockedModel, times(1)).result();
        verifyZeroInteractions(mockedView);
    }


}
