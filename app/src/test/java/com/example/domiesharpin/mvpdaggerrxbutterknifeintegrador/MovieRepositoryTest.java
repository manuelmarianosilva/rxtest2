package com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador;

import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.Http.OMDbAPIService;
import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.Http.TMDbAPIService;
import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.MainActivityElements.CustomViewModel;
import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.MainActivityElements.MovieRepository;
import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.Model.Movie;
import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.Model.MovieContainer;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieRepositoryTest {

    private final String TMDB_API_KEY = "bf55d24e465b3fb8dd1800b20fefff34";
    private final String OMDB_API_KEY = "d46ceacd";

    private TMDbAPIService mockedTMDbAPIService;
    private OMDbAPIService mockedOMDBAPIService;

    private Observable<MovieContainer> movieContainerObservable;
    private MovieContainer movieContainer;
    private Movie movie;

    private List<Movie> movieCache;
    private List<String> countryCache;

    private long lastTimeStamp;

    private static final long CACHE_LIFETIME = 20 * 1000;

    private MovieRepository movieRepository;

    @Before
    public void initTest(){

        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) {
                return Schedulers.trampoline();
            }
        });

        movie = new Movie();
        movie.setTitle("Cacho Lives");
        List<Movie> movieList = new ArrayList<>();
        movieList.add(movie);

        movieContainer = new MovieContainer();

        movieContainer.setResults(movieList);

        mockedTMDbAPIService = mock(TMDbAPIService.class);
        mockedOMDBAPIService = mock(OMDbAPIService.class);

        movieContainerObservable = Observable.just(movieContainer);

        movieCache = new ArrayList<>();
        countryCache = new ArrayList<>();

        lastTimeStamp = System.currentTimeMillis();
        movieRepository = new MovieRepository(mockedTMDbAPIService, mockedOMDBAPIService);
    }

    @Test
    public void checkNetworkCallAndEmptyResult(){
        TestObserver<Movie> testObserver = new TestObserver<>();

        when(mockedTMDbAPIService.getTopRatedMovies(TMDB_API_KEY, "es_AR", 1))
                .thenReturn(Observable.never());

        Observable<Movie> movieObservable = movieRepository.getMoviesFromTMDb();

        verify(mockedTMDbAPIService, times(1)).getTopRatedMovies(TMDB_API_KEY, "es_AR", 1);

        movieObservable.subscribe(testObserver);
        testObserver.assertEmpty();
        testObserver.assertNotComplete();
        testObserver.assertNotTerminated();
    }

    @Test
    public void checkNetworkCallAndFullResult(){
        TestObserver<Movie> testObserver = new TestObserver<>();

        when(mockedTMDbAPIService.getTopRatedMovies(TMDB_API_KEY, "es_AR", 1))
                .thenReturn(movieContainerObservable);

        Observable<Movie> movieObservable = movieRepository.getMoviesFromTMDb();

        verify(mockedTMDbAPIService, times(1)).getTopRatedMovies(TMDB_API_KEY, "es_AR", 1);

        movieObservable.subscribe(testObserver);
        testObserver.assertValue(movie1 -> movie1.getTitle().equals("Cacho Lives"));
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
    }

}
