package com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador;

import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.MainActivityElements.CustomViewModel;
import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.MainActivityElements.MainActivityModel;
import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.MainActivityElements.MovieRepository;
import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.MainActivityElements.Repository;
import com.example.domiesharpin.mvpdaggerrxbutterknifeintegrador.Model.Movie;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class ModelUnitTest {

    private Repository mockedRepository;
    private MainActivityModel mainActivityModel;

    private Observable<Movie> movieObservable;
    private Observable<String> stringObservable;

    private TestObserver<CustomViewModel> testObserver;


    @Before
    public void initializeTest(){

        mockedRepository = mock(MovieRepository.class);

        Movie movie = new Movie();
        movie.setTitle("Cacho Lives");
        String country = "González Catán";

        movieObservable = Observable.just(movie);
        stringObservable = Observable.just(country);

        testObserver = new TestObserver<>();

        mainActivityModel = new MainActivityModel(mockedRepository);

    }


    @Test
    public void checkThatCallIsMadeAndOutputObservableIsZipped(){

        when(mockedRepository.getMoviesFromTMDb()).thenReturn(movieObservable);
        when(mockedRepository.getCountry()).thenReturn(stringObservable);

        Observable<CustomViewModel> customViewModelObservable = mainActivityModel.result();

        verify(mockedRepository, times(1)).getMoviesFromTMDb();
        verify(mockedRepository, times(1)).getCountry();

        customViewModelObservable.subscribe(testObserver);
        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertValue(customViewModel ->
                customViewModel.getCountry().equals("González Catán") && customViewModel.getName().equals("Cacho Lives"));

    }

    @Test
    public void checkNoOutputWithoutInputFromRetrofit(){

        when(mockedRepository.getMoviesFromTMDb()).thenReturn(Observable.never());
        when(mockedRepository.getCountry()).thenReturn(Observable.never());

        Observable<CustomViewModel> customViewModelObservable = mainActivityModel.result();

        verify(mockedRepository, times(1)).getMoviesFromTMDb();
        verify(mockedRepository, times(1)).getCountry();

        customViewModelObservable.subscribe(testObserver);
        testObserver.assertNoErrors();
        testObserver.assertNotComplete();
        testObserver.assertEmpty();
    }

}
